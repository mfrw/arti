# Semver tracking

This is a helpful file that we use for checking which crates will have
breaking or nonbreaking API changes in the next release of Arti.

For each crate, please write "BREAKING" if there is an API change that counts
as breaking in semver, and "MODIFIED" if there is a backward-compatible API
change.

You can change from MODIFIED to BREAKING, but never from BREAKING to
MODIFIED.

You don't need to list details; this isn't the changelog.

Don't document other changes in this file.

We can delete older sections here after we bump the releases.


## Since Arti 0.0.2

tor-proto: MODIFIED
  (New constructor for HsNtorClientInput)

tor-netdoc: BREAKING
  (No more tap_key function in MicrodescBuilder)

arti-client: MODIFIED
  (PartialEq, Eq for TorAddrError)
  (New configurable stuff)

tor-circmgr: MODIFIED
  New configuration options.

arti-config: MODIFIED
  New configuration options.

tor-units: MODIFIED
  (Eq,PartialEq for BoundedInt32)

caret: BREAKING
  (Removed unused-but-exported Error type.)

tor-linkspec: BREAKING
  (Removed to_owned() on ChanTarget)

tor-rtcompat: BREAKING
  (TlsConnector API changed)

tor-netdir: MODIFIED:
  (New functions for families, overrides.)
